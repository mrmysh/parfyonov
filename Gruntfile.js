module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Удалить production-файлы
        clean: {
            js: {
                src: [
                    'assets/js/main.min.js'
                ],
                options: {
                    force: true
                }
            },
            css: {
                src: [
                    'assets/css/main.min.css'
                ],
                options: {
                    force: true
                }
            }
        },

        // Минимизировать js-айл
        uglify: {
            build: {
                src: 'assets/js/main.js',
                dest: 'assets/js/main.min.js'
            }
        },

        // SASS -> CSS
        sass: {
            dist: {
                options: {
                    style: 'expanded',
                    compass: true
                },
                files: [{
                    expand: true,
                    cwd: 'assets/scss/',
                    src: ['*.scss'],
                    dest: 'assets/css/',
                    ext: '.css'
                }]
            }
        },

        // Минимизировать css-файл
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'assets/css/',
                    src: ['*.css', '!*.min.css'],
                    dest: 'assets/css/',
                    ext: '.min.css'
                }]
            }
        },

        // Наблюдать за изменением scss и js-файлов
        watch: {
            sass: {
                files: ['assets/scss/**/*.scss'],
                tasks: ['sass']
            }
        }

    });

    // Подключить плагины Grunt
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // Установить задачи по умолчанию
    grunt.registerTask('default', ['css']);
    grunt.registerTask('js', ['clean:js', 'uglify']);
    grunt.registerTask('css', ['sass', 'watch:sass']);

};
