Описание задания:
1. Для разрешений 1280 пикселей и выше используется макет interview_1280.psd.
2. Для разрешений меньше 1280 пикселей используется макет interview_960.psd.
3. В шапке сайта изображение заполняет весь блок и располагается по центру.
4. Контент страницы может быть от 1 до 1000 строк.
5. Количество пунктов и подпунктов правого меню может варьироваться в диапазоне от 1 до 10.
6. Эффект для разворачивания подменю выбирается самостоятельно.
7. Боковое меню является фиксированным при прокрутке до тех пор, пока блок соцсетей не оказывается на расстоянии
50 пикселей от футера. При этом следует учитывать различные размеры меню, контента и экрана.
8. Шрифт можно использовать только один - firasans (приложен).