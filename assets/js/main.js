var APP = (function ($, window, document, undefined) {
    $(document).ready(function () {
        APP.go();
    });

    var duration = 200;

    return {
        go: function () {
            var i, j = this.init;

            for (i in j) {
                j.hasOwnProperty(i) && j[i]();
            }
        },

        init: {
            initScrollUp: function () {
                $('.scrollup').on('click', function () {
                    $('body,html').animate({
                        scrollTop: 0
                    }, duration);
                    return false;
                });
            }

            , initMunu: function () {
                $('.toggle').on('click', function () {
                    var $item = $(this).parent();

                    $item.children('.menu-toggle')
                        .slideToggle(duration, function () {
                            $(document.body).trigger("sticky_kit:recalc");
                            $item.toggleClass('open');
                        });
                    return false;
                });
            }

            , initSidebar: function () {
                $('.stick').stick_in_parent({
                    parent: '.layout'
                    //, recalc_every: 1
                });
            }
        }
    };
})(jQuery, this, this.document);
